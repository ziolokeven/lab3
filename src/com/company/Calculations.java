package com.company;

public class Calculations {

    static public Point2D positionGeometricCenter(Point2D[ ] point){
        double x = 0;
        double y = 0;

        for(int i = 0; i < point.length; i++){
            x += point[i].x;
            y+= point[i].y;
        }

        return new Point2D(x/point.length, y/point.length);
    }

    static public Point2D positionCenterOfMass(MaterialPoint2D[ ] materialPoint){
        double x = 0;
        double y = 0;
        double mass = 0;

        for(int i = 0; i < materialPoint.length; i++){
            x += materialPoint[i].mass * materialPoint[i].x;
            y += materialPoint[i].mass * materialPoint[i].y;
            mass += materialPoint[i].mass;
        }

        return new MaterialPoint2D(x/mass, y/mass, mass);
    }
}
